# frozen_string_literal: true

module License
  module Management
    module Report
      class V2_1 < V2
        def to_h
          super.merge(version: '2.1')
        end

        private

        def license_summary
          all_licenses
            .map { |license| data_for(license) }
            .uniq { |data| data['id'] }
            .sort_by { |data| data['id'] }
        end

        def map_from(dependency)
          {
            name: dependency.name,
            version: dependency.version,
            package_manager: dependency.package_manager.downcase.to_sym,
            path: detection_path_for(dependency),
            licenses: licenses_for(dependency)
          }
        end

        def detection_path_for(dependency)
          dependency = dependency.dependency if dependency.instance_of?(LicenseFinder::MergedPackage)

          if dependency.respond_to?(:detection_path)
            dependency.detection_path.relative_path_from(Pathname.pwd).to_s
          else
            '.'
          end
        end
      end
    end
  end
end
