# frozen_string_literal: true

module LicenseFinder
  class Dotnet
    def possible_package_paths
      project_path.glob('*.sln') +
        project_path.glob('*.csproj') +
        project_path.glob('*.vbproj') +
        project_path.glob('*.fsproj')
    end

    def installed?(*)
      true
    end

    def prepare
      create_vendor_path
      tool_box.install(tool: :"dotnet-core", version: dotnet_version)

      shell.execute([
        "/opt/asdf/installs/dotnet-core/#{dotnet_version}/dotnet",
        :restore, detected_package_path,
        '--locked-mode',
        '--no-cache',
        '--packages', vendor_path,
        '--verbosity', :normal
      ], capture: false)
    end

    def current_packages
      asset_files.flat_map do |file|
        json = JSON.parse(IO.read(file))
        json.fetch('libraries', []).map do |slug, data|
          name, version = slug.split('/')
          map_from(name, version, data)
        end
      end
    end

    def dotnet_version
      @dotnet_version ||= tool_box.version_of(:"dotnet-core")
    end

    private

    def map_from(name, version, data)
      Dependency.new(
        'NuGet',
        name,
        version,
        spec_licenses: licenses_from(data['path'], data.fetch('files', [])),
        detection_path: detected_package_path,
        install_path: vendor_path.join(data['path'])
      )
    end

    def nuspec_path_from(path, files)
      install_path = vendor_path.join(path)
      nuspec_filename = files.find { |x| x.end_with?('.nuspec') }
      return install_path.join(nuspec_filename) if nuspec_filename
    end

    def licenses_from(path, files)
      nuspec = nuspec_path_from(path, files)
      return [] if nuspec.nil? || !nuspec.exist?

      ::License::Management::Nuspec.new(nuspec.read).licenses
    end
  end
end
